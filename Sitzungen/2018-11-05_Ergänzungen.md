# Ergänzungen zur Sitzung am 5. November 2018

## Orientierungstest 2b)

Zu beweisen ist die Behauptung $A^n \subseteq (A \cup B)^n$ für zwei Mengen $A$ und $B$ und $n \in \mathbb{N}_{\gt 0}$

### Induktionsanfang

Sei $n=1$.
$$
A^1 \subseteq (A \cup B)^1 = A \subseteq A \cup B
$$
Dies gilt gemäß der Definition von Vereinigung.

### Induktionsvoraussetzung

Wir können also für alle zu beweisenden $n\leq 1$ die Behauptung bestätigen.
$$
A^n \subseteq (A \cup B)^n
$$

### Induktionsschritt

Für alle $n>1$ wird nun gezeigt, dass sie induktiv aus der Induktionsvoraussetzung folgen.
$$
A^{n+1} = A^n \cdot A\\
(A\cup B)^{n+1} = (A\cup B)^n \cdot (A\cup B)
$$
Wir nennen $A^n=C$, $A=D$, $(A\cup B)^n=E$ und $(A\cup B)=F$.

$C\subseteq E$ und $A\subseteq F$ entsprechen der Induktionsvoraussetzung bzw. dem Induktionsanfang. Schließlich wurde in der Übung der Beweis für den Induktionsschritt erbracht:
$$
C\subseteq E \wedge A\subseteq F \Rightarrow CA \subseteq EF
$$
$\square{}$

## Zu Übungsblatt 2.5

Der besprochene Algorithmus funktioniert folgendermaßen.

1. $T_0$ ist eine Menge, welche nur das Startsymbol enthält.

2. Um $T_{n+1}$ zu erhalten, werden alle möglichen Ableitungsschritte an allen Teilableitungen in $T_n$ durchgeführt.

   Ist z.B. $T_n=\{S,aBcd,aSb,aA,abba\}$ und die Produktionsregeln $P=\{S\rightarrow aBcd | aSb,A\rightarrow bba,B\rightarrow bb\}$. Dann enthält $T_{n+1}=T_n\cup \{aBcd, aSb, abbcd, aaBcdb, aaSbb, abba\}$. Da Mengen keine Elemente doppelt enthalten, kommen tatsächlich nur $abbcd$, $aaBcdc$ und $aaSbb$ hinzu.

3. Ist irgendwann $T_n=T_{n+1}$ bricht der Algorithmus ab. (Das muss nie zwingend passieren!)

Uns muss bewusst sein, dass deshalb potenziell in jedem Ableitungsschritt $|T_n|\cdot |P|$ neue Teilableitungen hinzukommen können.